/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function printWelcomeMessage(){
		let userName = prompt("What is your name?");
		let userAge = prompt("How old are you?");
		let userLocation = prompt("Where do you live?");

		console.log("Hello, " + userName);
		console.log("You are " + userAge + " years old.");
		console.log("You live in " + userLocation);
		alert("Thank you for your input!");
	};

	printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function showFaveBands(){

		let faveBand1 = "1. I Belong To The Zoo";
		let faveBand2 = "2. Boys Like Girls";
		let faveBand3 = "3. December Avenue";
		let faveBand4 = "4. MYMP";
		let faveBand5 = "5. Sponge Cola";

		console.log(faveBand1);
		console.log(faveBand2);
		console.log(faveBand3);
		console.log(faveBand4);
		console.log(faveBand5);
	};

	showFaveBands();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function showFaveMovies(){

		let faveMovie1 = "1. Hidden Figures";
		let movieRating1 = "Rotten Tomatoes Rating: 99%"
		let faveMovie2 = "2. Forgotten";
		let movieRating2 = "Rotten Tomatoes Rating: 90%"
		let faveMovie3 = "3. Taken";
		let movieRating3 = "Rotten Tomatoes Rating: 92%"
		let faveMovie4 = "4. The Vow";
		let movieRating4 = "Rotten Tomatoes Rating: 88%"		
		let faveMovie5 = "5. The Amityville Horror (2005)";
		let movieRating5 = "Rotten Tomatoes Rating: 85%"

		console.log(faveMovie1);
		console.log(movieRating1);
		console.log(faveMovie2);
		console.log(movieRating2);
		console.log(faveMovie3);
		console.log(movieRating3);
		console.log(faveMovie4);
		console.log(movieRating4);
		console.log(faveMovie5);
		console.log(movieRating5);
	};

	showFaveMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();